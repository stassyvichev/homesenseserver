<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.List" %>
<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="com.google.appengine.api.users.UserService" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>
<%@ page import="com.google.appengine.api.datastore.DatastoreServiceFactory" %>
<%@ page import="com.google.appengine.api.datastore.DatastoreService" %>
<%@ page import="com.google.appengine.api.datastore.Query" %>
<%@ page import="com.google.appengine.api.datastore.Entity" %>
<%@ page import="com.google.appengine.api.datastore.FetchOptions" %>
<%@ page import="com.google.appengine.api.datastore.Key" %>
<%@ page import="com.google.appengine.api.datastore.KeyFactory" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html>

  <body>
  
  <%
  String houseidName = "houseidName";
  	pageContext.setAttribute("houseidName", houseidName);
    DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
    Key houseidKey = KeyFactory.createKey("houseidName", houseidName);
    Query query = new Query("HouseReading", houseidKey).addSort("Ref_Number", Query.SortDirection.DESCENDING);
    List<Entity> housereadings = datastore.prepare(query).asList(FetchOptions.Builder.withLimit(5));
    
    if (housereadings.isEmpty()) {
        %>
        <p>There are no readings in the House Table.</p>
        <%
    } else {
    %>
        <p>The readings in the House Table are: </p>
        <table border="2">
	        	<tr>
	        		<td>Ref_Number</td>
	        		<td>Postcode</td>
				</tr>
        <%
     for (Entity housereading : housereadings) {
    pageContext.setAttribute("housereading_Ref_Number",
                          		housereading.getProperty("Ref_Number"));
   	pageContext.setAttribute("housereading_Postcode",
                          		housereading.getProperty("Postcode"));
                          	%>	
                    <tr>
		                <td>${fn:escapeXml(housereading_Ref_Number)}</td>
		       			<td>${fn:escapeXml(housereading_Postcode)}</td>
		            </tr>
		              <%
		        }
		        %>
		    </table>
	    <%  
	    }
	%>
	<p> </p>
               <form action="/house" method="post">
		<table border="1">
			<tr>
		    	<td>Ref_Number</td>
		    	<td>Postcode</td>
		    	<td></td>
		    </tr>
		    <tr>
		    	<td><textarea name="Ref_Number"></textarea></td>
		    	<td><textarea name="Postcode"></textarea></td>
		    	<td><input type="submit" value="Post Reading" /></td>
		    </tr>
		</table>
		<input type="hidden" name="houseidName" value="${fn:escapeXml(houseidName)}"/>
	</form>          		
    </body>
</html>
    
    
    