<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.List" %>
<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="com.google.appengine.api.users.UserService" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>
<%@ page import="com.google.appengine.api.datastore.DatastoreServiceFactory" %>
<%@ page import="com.google.appengine.api.datastore.DatastoreService" %>
<%@ page import="com.google.appengine.api.datastore.Query" %>
<%@ page import="com.google.appengine.api.datastore.Entity" %>
<%@ page import="com.google.appengine.api.datastore.FetchOptions" %>
<%@ page import="com.google.appengine.api.datastore.Key" %>
<%@ page import="com.google.appengine.api.datastore.KeyFactory" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html>

  <body>

	<%
		String registrationIDs = "registrationIDs";
	    pageContext.setAttribute("registrationIDs", registrationIDs);
	    DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	    Key registrationIDsKey = KeyFactory.createKey("registrationIDs", registrationIDs);
	    // Run an ancestor query to ensure we see the most up-to-date
	    // view of the sensor readings belonging to the selected SensorReadings entity.
	    Query query = new Query("RegistrationID", registrationIDsKey).addSort("date", Query.SortDirection.DESCENDING);
	    List<Entity> regIDs = datastore.prepare(query).asList(FetchOptions.Builder.withLimit(5));
	    if (regIDs.isEmpty()) {
	        %>
	        <p>No registration IDs.</p>
	        <%
	    } else {
	        %>
	        <p>Registration IDs</p>
	        <table border="1">
	        	<tr>
	        		<td>Registration ID</td>
	        		<td>Reading Date</td>
				</tr>
		        <%
		        for (Entity regID : regIDs) {
		            pageContext.setAttribute("regID_date",
		                                     regID.getProperty("date"));
		            pageContext.setAttribute("regID_regID",
		                                     regID.getProperty("regID"));
		            %>
		            <tr>
		                <td>${fn:escapeXml(regID_regID)}</td>
		                <td>${fn:escapeXml(regID_date)}</td>
		            </tr>
	            <%
		        }
		        %>
		    </table>
	    <%  
	    }
	%>
	</body>
</html>