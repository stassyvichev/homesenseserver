<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.List" %>
<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="com.google.appengine.api.users.UserService" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>
<%@ page import="com.google.appengine.api.datastore.DatastoreServiceFactory" %>
<%@ page import="com.google.appengine.api.datastore.DatastoreService" %>
<%@ page import="com.google.appengine.api.datastore.Query" %>
<%@ page import="com.google.appengine.api.datastore.Entity" %>
<%@ page import="com.google.appengine.api.datastore.FetchOptions" %>
<%@ page import="com.google.appengine.api.datastore.Key" %>
<%@ page import="com.google.appengine.api.datastore.KeyFactory" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html>
	
  	<body>
	  	<head>
			<title>Sending Test</title>
			<link rel='icon' href='favicon.png'/>
		</head>
		<%
			String registrationIDs = "registrationIDs";
		    String regID="regID";
		    DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		    Key registrationIDsKey = KeyFactory.createKey("registrationIDs", registrationIDs);
		    Query query = new Query("RegistrationID", registrationIDsKey).addSort("date", Query.SortDirection.DESCENDING);
		    List<Entity> regIDs = datastore.prepare(query).asList(FetchOptions.Builder.withLimit(5));
		    if (regIDs.isEmpty()) { 
		    %>
			     <h2>No devices registered!</h2>
		<%
			} else {
			
				pageContext.setAttribute("regIDs_size",
		                                    regIDs.size());
  				pageContext.setAttribute("regID_latest",
		                                    regIDs.get(0).getProperty(regID));
		      %>
			      <h2>${fn:escapeXml(regIDs_size)} device(s) registered!</h2>
			      <h2> Most recent registration ID:</h2>
			      <form name='form' method='POST' action='sendsingle'>
			      	<textarea name=device >${fn:escapeXml(regID_latest)}</textarea>
			      	<textarea name=msg >Please Check your smart meter!</textarea>
			      	<input type='submit' value='Send Message' />
			      </form>
		<%
			  }
			  %>
	</body>
</html>
    
	