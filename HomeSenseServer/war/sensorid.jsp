<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.List" %>
<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="com.google.appengine.api.users.UserService" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>
<%@ page import="com.google.appengine.api.datastore.DatastoreServiceFactory" %>
<%@ page import="com.google.appengine.api.datastore.DatastoreService" %>
<%@ page import="com.google.appengine.api.datastore.Query" %>
<%@ page import="com.google.appengine.api.datastore.Entity" %>
<%@ page import="com.google.appengine.api.datastore.FetchOptions" %>
<%@ page import="com.google.appengine.api.datastore.Key" %>
<%@ page import="com.google.appengine.api.datastore.KeyFactory" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html>

  <body>
  
  <%
  String sensoridName = "sensoridName";
  	pageContext.setAttribute("sensoridName", sensoridName);
    DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
    Key sensoridKey = KeyFactory.createKey("sensoridName", sensoridName);
    Query query = new Query("SensorIDReading", sensoridKey);
    List<Entity> sensoridreadings = datastore.prepare(query).asList(FetchOptions.Builder.withLimit(5));
    
    if (sensoridreadings.isEmpty()) {
        %>
        <p>There are no readings in the Sensor Table.</p>
        <%
    } else {
    %>
        <p>The readings in the Sensor Table are: </p>
        <table border="2">
	        	<tr>
	        		<td>SensorID</td>
	        		<td>Ref_Number</td>
				</tr>
        <%
     for (Entity sensoridreading : sensoridreadings) {
     	pageContext.setAttribute("sensoridreading_SensorID",
                          		sensoridreading.getProperty("SensorID"));
    pageContext.setAttribute("sensoridreading_Ref_Number",
                          		sensoridreading.getProperty("Ref_Number"));
                          	%>	
                    <tr>
                    <td>${fn:escapeXml(sensoridreading_SensorID)}</td>
		            <td>${fn:escapeXml(sensoridreading_Ref_Number)}</td>	
		            </tr>
		              <%
		        }
		        %>
		    </table>
	    <%  
	    }
	%>
	<p> </p>
               <form action="/sensorid" method="post">
		<table border="1">
			<tr>
				<td>SensorID</td>
		    	<td>Ref_Number</td>
		    </tr>
		    <tr>
		    	<td><textarea name="SensorID"></textarea></td>
		    	<td><textarea name="Ref_Number"></textarea></td>
		    	<td><input type="submit" value="Post Reading" /></td>
		    </tr>
		</table>
		<input type="hidden" name="sensoridName" value="${fn:escapeXml(sensoridName)}"/>
	</form>          		
    </body>
</html>
    
    
    