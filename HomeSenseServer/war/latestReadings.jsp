<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="com.google.appengine.api.users.UserService" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>
<%@ page import="com.google.appengine.api.datastore.DatastoreServiceFactory" %>
<%@ page import="com.google.appengine.api.datastore.DatastoreService" %>
<%@ page import="com.google.appengine.api.datastore.Query" %>
<%@ page import="com.google.appengine.api.datastore.Entity" %>
<%@ page import="com.google.appengine.api.datastore.FetchOptions" %>
<%@ page import="com.google.appengine.api.datastore.Key" %>
<%@ page import="com.google.appengine.api.datastore.KeyFactory" %>
<%@ page import="com.google.appengine.api.datastore.Query.Filter" %>
<%@ page import="com.google.appengine.api.datastore.Query.FilterOperator" %>
<%@ page import="com.google.appengine.api.datastore.Query.FilterPredicate" %>
<%@ page import="com.google.appengine.api.datastore.PreparedQuery" %>
<%@ page import="com.google.appengine.api.datastore.PropertyProjection" %>
<%@ page import="com.google.appengine.api.datastore.RawValue" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html>
	<body>
  
  <%
  	String Readingstatus = request.getParameter("Readingstatus");
  	String sensorReadings="sensorReadings";
  	pageContext.setAttribute("sensorReadings", sensorReadings);
  	Key sensorReadingsKey = KeyFactory.createKey("sensorReadings", sensorReadings);
  	DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
  	Query q = new Query("Reading", sensorReadingsKey);
  	q.addProjection(new PropertyProjection("sensorID", String.class)).setDistinct(true);
    q.addProjection(new PropertyProjection("date", Date.class));
	q.addProjection(new PropertyProjection("value", String.class));
	q.addSort("sensorID", Query.SortDirection.ASCENDING);
    q.addSort("date", Query.SortDirection.DESCENDING);
    List<Entity> sensorreadings1 = datastore.prepare(q).asList(FetchOptions.Builder.withLimit(10));
     
     System.out.println(sensorreadings1.size());
    List<Entity> table1= new ArrayList<Entity>();
    ArrayList<String> sensornumber= new ArrayList<String>();
    for (Entity sensorreadings : sensorreadings1){
    	String sensoridvalue= (String) sensorreadings.getProperty("sensorID");
    	System.out.println(sensoridvalue);
    	if(sensornumber.contains(sensoridvalue)==false){
    		sensornumber.add(sensoridvalue);
    		table1.add(sensorreadings);
    	}
    }
    
     %>
  <p>Latest Readings for all SensorID's.</p>
	
	<table border="2">
	        	<tr>
	        		<td>SensorID</td>
	        		<td>date</td>
	        		<td>Status</td>
				</tr>
				
				<%  
				for (Entity reading : table1) {
   
    pageContext.setAttribute("reading_sensorID",
                          		reading.getProperty("sensorID"));
    pageContext.setAttribute("reading_date",
                          		reading.getProperty("date"));
   	
   	int i = Integer.parseInt(reading.getProperty("value").toString());           		
     				if (i > 22) {
        				Readingstatus = "High";
        			pageContext.setAttribute("reading_status", Readingstatus);
    				} else if ((i < 22) && (i > 18)){
    				Readingstatus = "Medium";
        			pageContext.setAttribute("reading_status", Readingstatus);
    				} else {
   	 					Readingstatus = "Low";
    				pageContext.setAttribute("reading_status", Readingstatus);  
    		}  
                          		 %>
                          		<tr>
		                <td>${fn:escapeXml(reading_sensorID)}</td>
		                <td>${fn:escapeXml(reading_date)}</td>
		                <td>${fn:escapeXml(reading_status)}</td>
		            </tr>
		            <%
	              	 }	
	              	 %>
	        </table>  	
  
   </body>
</html>