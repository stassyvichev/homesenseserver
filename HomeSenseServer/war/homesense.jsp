<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.List" %>
<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="com.google.appengine.api.users.UserService" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>
<%@ page import="com.google.appengine.api.datastore.DatastoreServiceFactory" %>
<%@ page import="com.google.appengine.api.datastore.DatastoreService" %>
<%@ page import="com.google.appengine.api.datastore.Query" %>
<%@ page import="com.google.appengine.api.datastore.Entity" %>
<%@ page import="com.google.appengine.api.datastore.FetchOptions" %>
<%@ page import="com.google.appengine.api.datastore.Key" %>
<%@ page import="com.google.appengine.api.datastore.KeyFactory" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html>

  <body>

	<%
		String sensorReadings = "sensorReadings";
	    pageContext.setAttribute("sensorReadings", sensorReadings);
	    DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	    Key sensorReadingsKey = KeyFactory.createKey("sensorReadings", sensorReadings);
	    // Run an ancestor query to ensure we see the most up-to-date
	    // view of the sensor readings belonging to the selected SensorReadings entity.
	    Query query = new Query("Reading", sensorReadingsKey).addSort("date", Query.SortDirection.DESCENDING);
	    List<Entity> readings = datastore.prepare(query).asList(FetchOptions.Builder.withLimit(10));
	    if (readings.isEmpty()) {
	        %>
	        <p>No sensor readings.</p>
	        <%
	    } else {
	        %>
	        <p>Sensor Readings</p>
	        <table border="2">
	        	<tr>
	        		<td>Sensor ID</td>
	        		<td>Reading Date</td>
					<td>Reading Value</td>
				</tr>
		        <%
		        for (Entity reading : readings) {
		            pageContext.setAttribute("reading_value",
		                                     reading.getProperty("value"));
		            pageContext.setAttribute("reading_date",
		                                     reading.getProperty("date"));
		            pageContext.setAttribute("reading_sensorID",
		                                     reading.getProperty("sensorID"));
		            %>
		            <tr>
		                <td>${fn:escapeXml(reading_sensorID)}</td>
		                <td>${fn:escapeXml(reading_date)}</td>
		                <td>${fn:escapeXml(reading_value)}</td>
		            </tr>
	            <%
		        }
		        %>
		    </table>
	    <%  
	    }
	%>
	<p></p>
	<form action="/sign" method="post">
		<table border="1">
			<tr>
		    	<td>Sensor ID</td>
		    	<td>Value</td>
		    	<td></td>
		    </tr>
		    <tr>
		    	<td><textarea name="sensorID"></textarea></td>
		    	<td><textarea name="value"></textarea></td>
		    	<td><input type="submit" value="Post Reading" /></td>
		    </tr>
		</table>
		<input type="hidden" name="sensorReadings" value="${fn:escapeXml(sensorReadings)}"/>
	</form>
  </body>
</html>