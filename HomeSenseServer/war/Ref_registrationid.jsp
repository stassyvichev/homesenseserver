<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.List" %>
<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="com.google.appengine.api.users.UserService" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>
<%@ page import="com.google.appengine.api.datastore.DatastoreServiceFactory" %>
<%@ page import="com.google.appengine.api.datastore.DatastoreService" %>
<%@ page import="com.google.appengine.api.datastore.Query" %>
<%@ page import="com.google.appengine.api.datastore.Entity" %>
<%@ page import="com.google.appengine.api.datastore.FetchOptions" %>
<%@ page import="com.google.appengine.api.datastore.Key" %>
<%@ page import="com.google.appengine.api.datastore.KeyFactory" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html>

  <body>
  
  <%
  String registrationidName = "registrationidName";
  	pageContext.setAttribute("registrationidName", registrationidName);
    DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
    Key registrationidKey = KeyFactory.createKey("registrationidName", registrationidName);
    Query query = new Query("RegistrationReading", registrationidKey);
    List<Entity> registrationreadings = datastore.prepare(query).asList(FetchOptions.Builder.withLimit(5));
    
    if (registrationreadings.isEmpty()) {
        %>
        <p>There are no readings in the Registration Table.</p>
        <%
    } else {
    %>
        <p>The readings in the Registration Table are: </p>
        <table border="2">
	        	<tr>
	        		<td>RegistrationID</td>
	        		<td>Ref_Number</td>
				</tr>
        <%
     for (Entity registrationreading : registrationreadings) {
    	pageContext.setAttribute("registrationreading_RegistrationID",
                          		registrationreading.getProperty("RegistrationID"));
    pageContext.setAttribute("registrationreading_Ref_Number",
                          		registrationreading.getProperty("Ref_Number"));
                          	%>	
                    <tr>
		                <td>${fn:escapeXml(registrationreading_RegistrationID)}</td>
		                <td>${fn:escapeXml(registrationreading_Ref_Number)}</td>
		            </tr>
		              <%
		        }
		        %>
		    </table>
	    <%  
	    }
	%>
	<p> </p>
               <form action="/ref_registration" method="post">
		<table border="1">
			<tr>
				<td>RegistrationID</td>
		    	<td>Ref_Number</td>
		    </tr>
		    <tr>
		    	<td><textarea name="RegistrationID"></textarea></td>
		    	<td><textarea name="Ref_Number"></textarea></td>
		    	<td><input type="submit" value="Post Reading" /></td>
		    </tr>
		</table>
		<input type="hidden" name="registrationidName" value="${fn:escapeXml(registrationidName)}"/>
	</form>          		
    </body>
</html>
    
    
    