/*
 * Copyright 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package messaging;

import static com.google.appengine.api.taskqueue.TaskOptions.Builder.withUrl;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;

/**
 * Servlet that adds display number of devices and button to send a message.
 * <p>
 * This servlet is used just by the browser (i.e., not device) and contains the
 * main page of the demo app.
 */
@SuppressWarnings("serial")
public class TestSendServlet extends BaseServlet {

  static final String ATTRIBUTE_STATUS = "status";

  /**
   * Displays the existing messages and offer the option to send a new one.
   */
  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
      throws IOException {
    resp.setContentType("text/html");
    PrintWriter out = resp.getWriter();

    out.print("<html><body>");
    out.print("<head>");
    out.print("  <title>Sending Test</title>");
    out.print("  <link rel='icon' href='favicon.png'/>");
    out.print("</head>");
    String status = (String) req.getAttribute(ATTRIBUTE_STATUS);
    if (status != null) {
      out.print(status);
    }
    
    String registrationIDs = "registrationIDs";
    String regID="regID";
    DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
    Key registrationIDsKey = KeyFactory.createKey("registrationIDs", registrationIDs);
    Query query = new Query("RegistrationID", registrationIDsKey).addSort("date", Query.SortDirection.DESCENDING);
    List<Entity> regIDs = datastore.prepare(query).asList(FetchOptions.Builder.withLimit(5));
    if (regIDs.isEmpty()) {
      out.print("<h2>No devices registered!</h2>");
    } else {
      out.print("<h2>" +regIDs.size() + " device(s) registered!</h2>");
      out.print("<h2> Most recent registration ID:</h2>");
      out.print("<form name='form' method='POST' action='send'>");
      out.print("<h2 name=device >"+regIDs.get(0).getProperty(regID) +"</h2>");
      out.print("<input type='submit' value='Send Message' />");
      out.print("</form>");
    }
    out.print("</body></html>");
    resp.setStatus(HttpServletResponse.SC_OK);
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
      throws IOException {
	logger.info("In doPost ");
    String regID=req.getParameter("device");
    String msg=req.getParameter("msg");
    logger.info("Attempting to send message to "+regID);
    Queue queue = QueueFactory.getQueue("hs");
    logger.info("Adding stuff to queue ");
    queue.add(withUrl("/send").param(
            SendMessageServlet.PARAMETER_DEVICE, regID).param(SendMessageServlet.PARAMETER_MESSAGE, msg));
    
  }

}
