package homesense;

import static com.google.appengine.api.taskqueue.TaskOptions.Builder.withUrl;

import java.io.IOException;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.*;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;

@SuppressWarnings("serial")
public class ReadingReceiverServlet extends HttpServlet{
	protected final Logger logger = Logger.getLogger(getClass().getName());
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException
    {
		//server receives reading from twine block
		//currently set to orientation
		logger.info("Reached Servlet");
		Enumeration<?> parameters= req.getParameterNames();
		while (parameters.hasMoreElements()){
			logger.info(parameters.nextElement().toString());
		}
		String orientation=req.getParameter("orientation");
		String temperature=req.getParameter("temperature");
		String sensorID= req.getParameter("sensor");
		Date date = new Date();
//		System.out.println(sensorID);
		//extract reading from http post
		String sensorReadings="sensorReadings";
        Key sensorReadingsKey = KeyFactory.createKey("sensorReadings", sensorReadings);
        Entity reading = new Entity("Reading", sensorReadingsKey);
        reading.setProperty("sensorID", sensorID);
        reading.setProperty("date", date);
        reading.setProperty("value", temperature);
        
        //save reading in data store
        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        datastore.put(reading);
    	
    	//get ref_id, which corresponds to this sensor ID
    	String sensoridName = "sensoridName";
    	Key sensoridKey = KeyFactory.createKey("sensoridName", sensoridName);
    	Filter sensorIDFilter =new FilterPredicate("SensorID", FilterOperator.EQUAL, sensorID);	
    	Query sensorQuery = new Query("SensorIDReading", sensoridKey).setFilter(sensorIDFilter);
    	List<Entity> sensorRefID = datastore.prepare(sensorQuery).asList(FetchOptions.Builder.withLimit(1));
    	String refNumber=(String)sensorRefID.get(0).getProperty("Ref_Number");
    	logger.info("The ref number is: "+refNumber);
    	
    	//get registrationID corresponding to refNumber
    	String registrationidName = "registrationidName";
    	Key registrationidKey = KeyFactory.createKey("registrationidName", registrationidName);
    	Filter regIDFilter =new FilterPredicate("Ref_Number", FilterOperator.EQUAL, refNumber);
    	Query regIDQuery = new Query("RegistrationReading", registrationidKey).setFilter(regIDFilter);
    	List<Entity> regRefID = datastore.prepare(regIDQuery).asList(FetchOptions.Builder.withLimit(1));
    	String registrationID=(String)regRefID.get(0).getProperty("RegistrationID");
//    	logger.info("The corresponding reg ID is: "+ registrationID);
    	
    	//get last added registration ID from data store
//    	String registrationIDs = "registrationIDs";
//    	String regID="regID";
//	    Key registrationIDsKey = KeyFactory.createKey("registrationIDs", registrationIDs);
//	    Query query = new Query("RegistrationID", registrationIDsKey).addSort("date", Query.SortDirection.DESCENDING);
//	    List<Entity> regIDs = datastore.prepare(query).asList(FetchOptions.Builder.withLimit(5));
//	    String latestRegID=regIDs.get(0).getProperty(regID).toString();
	    String msg="Temperature is "+temperature;
	    //open queue
//	    logger.info("Attempting to send message to "+latestRegID);
	    logger.info("Attempting to send message to "+registrationID);
	    Queue queue = QueueFactory.getQueue("hs");
	    //add message sending task to queue
//	    queue.add(withUrl("/send").param("device", latestRegID).param("msg", msg));
	    queue.add(withUrl("/send").param("device", registrationID).param("msg", msg));
    }
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException
    {
		Queue queue = QueueFactory.getQueue("hs");
	    //add message sending task to queue
	    queue.add(withUrl("/receivereading")
	    		.param("orientation", "top")
	    		.param("temperature", "24").param("sensor", "S_SMV"));
    }
}
