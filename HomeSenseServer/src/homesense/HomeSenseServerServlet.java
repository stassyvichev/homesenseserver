package homesense;

import java.io.IOException;
import javax.servlet.http.*;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

@SuppressWarnings("serial")
public class HomeSenseServerServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
//		resp.setContentType("text/plain");
//		resp.getWriter().println("Hello, HomeSense!");
		UserService userService = UserServiceFactory.getUserService();
        User user = userService.getCurrentUser();
// checks to see if a user has logged in. If they have then displays their name
        if (user != null) {
            resp.setContentType("text/plain");
            resp.getWriter().println("Hello, " + user.getNickname()); // Displays user name
            resp.setContentType("text/html");
            resp.getWriter().println("<p>You can <a href=\"" +
                    userService.createLogoutURL(req.getRequestURI()) +
                    "\">sign out</a>.</p>"); //Message that allows the user to logout
            
        } else { // If no one is logged in, user is redirected to Login page
            resp.sendRedirect(userService.createLoginURL(req.getRequestURI()));
        }

	}
}
