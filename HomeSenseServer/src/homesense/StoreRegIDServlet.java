package homesense;

import java.io.IOException;
import java.util.Date;
import java.util.logging.Logger;

import javax.servlet.http.*;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

@SuppressWarnings("serial")
public class StoreRegIDServlet extends HttpServlet {
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("text/html");
		resp.getWriter().println("StoreRegIDServlet");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException
    {
		String regID=req.getHeader("regid");
		Date date = new Date();
		System.out.println(regID);
//        LOG.info("doPost():  request test header:  " + request);
	    Key registrationIDsKey = KeyFactory.createKey("registrationIDs", "registrationIDs");
		Entity reading = new Entity("RegistrationID", registrationIDsKey);
        reading.setProperty("regID", regID);
        reading.setProperty("date", date);

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        datastore.put(reading);
    }
}