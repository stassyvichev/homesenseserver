package database;

import java.io.IOException;
import java.util.logging.Logger;
import javax.servlet.http.*;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import java.io.IOException;
import java.util.Date;



public class HouseIDServlet extends HttpServlet {

	@Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
                throws IOException {
        resp.sendRedirect("/houseid.jsp");
       }
	
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp)
                throws IOException {

//        String houseidName = req.getParameter("houseidName");
        String houseidName = "houseidName";
        Key houseidKey = KeyFactory.createKey("houseidName", houseidName);
        String RefNum = req.getParameter("Ref_Number");
        String Postcode = req.getParameter("Postcode");
        Date date = new Date();
        
        Entity housereading = new Entity("HouseReading", houseidKey);
        housereading.setProperty("Ref_Number", RefNum);
        housereading.setProperty("date", date);
        housereading.setProperty("Postcode", Postcode);

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        datastore.put(housereading);
        
//        resp.sendRedirect("/houseid.jsp?houseidName=" + houseidName);
        resp.sendRedirect("/houseid.jsp");
     
        
    }
}

