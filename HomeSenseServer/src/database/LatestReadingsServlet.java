package database;

import java.io.IOException;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import java.util.Date;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LatestReadingsServlet extends HttpServlet {

	public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws IOException {
    resp.sendRedirect("/latestReadings.jsp");
}
	
	@Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp)
                throws IOException {
		
		String sensorReadings="sensorReadings";
        Key sensorReadingsKey = KeyFactory.createKey("sensorReadings", sensorReadings);
        
        String value = req.getParameter("value");
        String sensorID = req.getParameter("sensorID");
        String status = req.getParameter("status");
        Date date = new Date();
        
        Entity reading = new Entity("Reading", sensorReadingsKey);
        reading.setProperty("sensorID", sensorID);
        reading.setProperty("date", date);
        reading.setProperty("value", value);
        reading.setProperty("status", status);

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        datastore.put(reading);
			
	}	
}
