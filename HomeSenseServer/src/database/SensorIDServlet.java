package database;

import java.io.IOException;
import java.util.logging.Logger;
import javax.servlet.http.*;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SensorIDServlet extends HttpServlet {

	@Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
                throws IOException {
        resp.sendRedirect("/sensorid.jsp");
    }
	@Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp)
                throws IOException {
    	
        String sensoridName = "sensoridName";
        Key sensoridKey = KeyFactory.createKey("sensoridName", sensoridName);
        String SensorID = req.getParameter("SensorID");
        String Ref_Number = req.getParameter("Ref_Number");
        
        Entity sensoridreading = new Entity("SensorIDReading", sensoridKey);
        sensoridreading.setProperty("SensorID", SensorID);
        sensoridreading.setProperty("Ref_Number", Ref_Number);
        

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        datastore.put(sensoridreading);

        resp.sendRedirect("/sensorid.jsp");
       
    }
}

