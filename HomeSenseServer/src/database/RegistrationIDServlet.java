package database;

import java.io.IOException;
import java.util.logging.Logger;
import javax.servlet.http.*;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RegistrationIDServlet extends HttpServlet {

	@Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
                throws IOException {
        resp.sendRedirect("/Ref_registrationid.jsp");
       
    }
	@Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp)
                throws IOException {
    	
        String registrationidName = "registrationidName";
        Key registrationidKey = KeyFactory.createKey("registrationidName", registrationidName);
        String RegistrationID = req.getParameter("RegistrationID");
        String Ref_Number = req.getParameter("Ref_Number");
        
        Entity registrationreading = new Entity("RegistrationReading", registrationidKey);
        registrationreading.setProperty("RegistrationID", RegistrationID);
        registrationreading.setProperty("Ref_Number", Ref_Number);
        

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        datastore.put(registrationreading);

        resp.sendRedirect("/Ref_registrationid.jsp");
       
    }
}

