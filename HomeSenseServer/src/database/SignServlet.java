package database;

import java.io.IOException;
import java.util.logging.Logger;
import javax.servlet.http.*;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import java.io.IOException;
import java.util.Date;


public class SignServlet extends HttpServlet {

	@Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
                throws IOException {
        resp.sendRedirect("/homesense.jsp"); // homesense page is loaded
    }
	@Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp)
                throws IOException {
        // Looks like this is where the values of the table are written into the table
        String sensorReadings="sensorReadings";
        Key sensorReadingsKey = KeyFactory.createKey("sensorReadings", sensorReadings);
        String value = req.getParameter("value");
        System.out.println("Hello " + value);
        String sensorID = req.getParameter("sensorID");
        String status = req.getParameter("status");
        Date date = new Date();
        Entity reading = new Entity("Reading", sensorReadingsKey);
        reading.setProperty("sensorID", sensorID);
        reading.setProperty("date", date);
        reading.setProperty("value", value);
        reading.setProperty("status", status);

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        datastore.put(reading);

        resp.sendRedirect("/homesense.jsp"); // homesense page is loaded

    }
}
